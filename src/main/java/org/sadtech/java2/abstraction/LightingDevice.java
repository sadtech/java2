package org.sadtech.java2.abstraction;

abstract public class LightingDevice extends Device {
    private int NumberLightBulb;
    private float PowerLightBuld;

    public LightingDevice(int NumberLightBulb, float PowerLightBuld) {
        super(NumberLightBulb * PowerLightBuld);
        this.NumberLightBulb = NumberLightBulb;
        this.PowerLightBuld = PowerLightBuld;
    }

    @Override
    public float getMaxPower() {
        return NumberLightBulb * PowerLightBuld;
    }

    public float getPowerLightBuld() {
        return PowerLightBuld;
    }
}
