package org.sadtech.java2.abstraction;

public interface Electronics {
    void turnON();

    void switchOFF();

    boolean isStatus();

    float getMaxPower();

    int getSerial();
}
