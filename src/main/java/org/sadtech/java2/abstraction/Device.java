package org.sadtech.java2.abstraction;

public class Device implements Electronics {
    protected float maxPower;
    protected int serial;
    protected boolean status;

    public Device(float power) {
        this.maxPower = power;
    }

    protected Device(float power, int serial) {
        this.maxPower = power;
        this.serial = serial;
    }

    public void turnON() {
        this.status = true;
    }

    public void switchOFF() {
        this.status = false;
    }

    public boolean isStatus() {
        return status;
    }

    public int getSerial() {
        return serial;
    }

    public float getMaxPower() {
        return maxPower;
    }

    @Override
    public String toString() {
        return "Device{" +
                "maxPower=" + maxPower +
                ", serial=" + serial +
                ", status=" + status +
                '}';
    }
}
