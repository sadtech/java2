package org.sadtech.java2.apartment.kitchen;

import org.sadtech.java2.abstraction.Device;

public class Kettle extends Device {

    public Kettle(float power, int serial) {
        super(power, serial);
    }

}
