package org.sadtech.java2.apartment.kitchen;

import org.sadtech.java2.abstraction.Device;

public class Fridge extends Device {

    private float temp, tempFreezer;

    public Fridge(float power, int serial) {
        super(power, serial);
        this.temp = 10;
        this.tempFreezer = 3;
    }

    @Override
    public float getMaxPower() {
        return (20 - this.temp + this.tempFreezer) * 100;
    }

    public void setTemp(float temp) {
        this.temp = temp;
    }

    public void setTempFreezer(float tempFreezer) {
        this.tempFreezer = tempFreezer;
    }

    public float getTemp() {
        return temp;
    }

    public float getTempFreezer() {
        return tempFreezer;
    }
}
