package org.sadtech.java2.apartment.kitchen;

import java.util.Scanner;

import org.sadtech.java2.abstraction.Device;

public class Plate extends Device {

    private Device[] burner;

    public Plate(int burnerCount, float burnerPower, int serial) {
        super(burnerPower * burnerCount, serial);
        this.burner = new Device[burnerCount];
        for (int i = 0; i < burnerCount; i++) {
            this.burner[i] = new Device(burnerPower);
        }
    }

    @Override
    public void turnON() {
        if (checkONBurner()) {
            Scanner in = new Scanner(System.in);
            System.out.println("Какую конфорку включить?");
            int number = in.nextInt();
            while (this.burner[number].isStatus()) {
                System.out.println("Конфорка уже включена, выберете другую");
                number = in.nextInt();
            }
            this.burner[number].turnON();
        } else {
            System.out.println("Все конфорки уже включены");
        }
    }

    @Override
    public void switchOFF() {
        if (checkOFFBurner()) {
            Scanner in = new Scanner(System.in);
            System.out.println("Какую конфорку выключить?");
            int number = in.nextInt();
            while (this.burner[number].isStatus() == false) {
                System.out.println("Конфорка уже выключена, выберете другую");
                number = in.nextInt();
            }
            this.burner[number].turnON();
        } else {
            System.out.println("Все конфорки выключены!");
        }
    }

    @Override
    public float getMaxPower() {
        return maxPower / burner.length;
    }

    @Override
    public boolean isStatus() {
        for (Device burner : this.burner) {
            if (burner.isStatus()) {
                return true;
            }
        }
        return false;
    }

    private boolean checkONBurner() {
        for (Device burner : this.burner) {
            if (burner.isStatus() == false) {
                return true;
            }
        }
        return false;
    }

    private boolean checkOFFBurner() {
        for (Device burner : this.burner) {
            if (burner.isStatus()) {
                return true;
            }
        }
        return false;
    }

}
