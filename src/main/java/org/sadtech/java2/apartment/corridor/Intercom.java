package org.sadtech.java2.apartment.corridor;

import org.sadtech.java2.abstraction.Device;

public class Intercom extends Device {

    private int Cod;

    public Intercom(int Cod, float power, int serial) {
        super(power, serial);
        this.Cod = Cod;
    }

    public int getCod() {
        return Cod;
    }

    public void openDoor() {
        System.out.println("Дверь открыта");
    }
}
