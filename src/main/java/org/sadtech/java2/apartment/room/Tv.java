package org.sadtech.java2.apartment.room;

import org.sadtech.java2.abstraction.Device;

public class Tv extends Device {

    private int channel;

    private enum inputs {AV, HDMI, DVD}

    private inputs input;

    public Tv(float power, int serial) {
        super(power, serial);
    }

    @Override
    public void turnON() {
        this.AV();
        this.channel = 1;
        this.status = true;
    }

    @Override
    public void switchOFF() {
        this.channel = 0;
        this.status = false;
    }

    public void setChanel(int channel) {
        this.channel = channel;
    }

    public int getChanel() {
        return channel;
    }

    public String getInput() {
        return input.toString();
    }

    public void AV() {
        this.input = inputs.AV;
        this.channel = 1;
    }

    public void HDMI() {
        this.input = inputs.HDMI;
        this.channel = 0;
    }

    public void DVD() {
        this.input = inputs.DVD;
        this.channel = 0;
    }

    @Override
    public String toString() {
        return "Device{" +
                "maxPower=" + maxPower +
                ", serial=" + serial +
                ", status=" + status +
                '}';
    }
}
