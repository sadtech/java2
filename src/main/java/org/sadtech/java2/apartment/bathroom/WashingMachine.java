package org.sadtech.java2.apartment.bathroom;

import org.sadtech.java2.abstraction.Device;

public class WashingMachine extends Device {

    private float Volume;

    public WashingMachine(float Volume, float power, int serial) {
        super(power, serial);
        this.Volume = Volume;
    }

    public float getVolume() {
        return Volume;
    }
}
