package org.sadtech.java2.apartment;

import org.sadtech.java2.abstraction.Electronics;
import org.sadtech.java2.apartment.corridor.Intercom;
import org.sadtech.java2.apartment.kitchen.Fridge;
import org.sadtech.java2.apartment.kitchen.Kettle;
import org.sadtech.java2.apartment.kitchen.Plate;
import org.sadtech.java2.apartment.room.Tv;
import org.sadtech.java2.comparators.MaxPowerComparator;
import org.sadtech.java2.comparators.SerialComparator;
import org.sadtech.java2.comparators.StatusComparator;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class SmartHome {

    public static void main(String[] arg) {
        SmartHome contr = new SmartHome();
        contr.runSmartHome();
    }

    public void runSmartHome() {
        Map<String, Electronics> deviceMap = new HashMap<>();
        deviceMap.put("LampBathroom", new Chandelier(2, 40));
        deviceMap.put("ChandelierCorridor", new Chandelier(1, 60));
        deviceMap.put("ChandelierKitchen", new Chandelier(2, 50));
        deviceMap.put("ChandelierRoom", new Chandelier(3, 50));
        deviceMap.put("WallChandelierRoom", new Chandelier(2, 60));
        deviceMap.put("Plate", new Plate(4, 500, 1));
        deviceMap.put("Tv", new Tv(100, 2));
        deviceMap.put("Intercom", new Intercom(22, 100, 3));
        deviceMap.put("Fridge", new Fridge(2000, 4));
        deviceMap.put("Kettle", new Kettle(500, 5));

        Controller controller = new Controller(deviceMap);

        controller.turnON("Intercom");
        controller.turnON("Fridge");

        controller.turnON("LampBathroom");
        controller.turnON("ChandelierCorridor");
        controller.turnON("ChandelierKitchen");
        controller.turnON("ChandelierRoom");

        controller.switchOFF("Plate");
        controller.turnON("Plate");
        controller.turnON("Plate");
        controller.switchOFF("Plate");

        controller.turnON("Tv");
        System.out.println(((Tv) (deviceMap.get("Tv"))).getChanel());
        ((Tv) (deviceMap.get("Tv"))).setChanel(12);
        System.out.println(((Tv) (deviceMap.get("Tv"))).getChanel());
        controller.turnON("Tv");
        controller.switchOFF("Tv");

        System.out.println("Текущая температура холодильной камеры: " + ((Fridge) (deviceMap.get("Fridge"))).getTemp());
        System.out.println("Текущая температура морозильной камеры: " + ((Fridge) (deviceMap.get("Fridge"))).getTempFreezer());
        ((Fridge) (deviceMap.get("Fridge"))).setTemp(16);
        ((Fridge) (deviceMap.get("Fridge"))).setTempFreezer(0);
        System.out.println("Новая температура холодильной камеры: " + ((Fridge) (deviceMap.get("Fridge"))).getTemp());
        System.out.println("Новая температура морозильной камеры: " + ((Fridge) (deviceMap.get("Fridge"))).getTempFreezer());

        ((Intercom) (deviceMap.get("Intercom"))).openDoor();

        controller.turnON("Kettle");

        System.out.println("Потребляемая мощность: " + controller.getMaxPower());

        System.out.println("\nСортировка по мощности:");
        Comparator maxPower = new MaxPowerComparator(deviceMap);
        Map<String, Electronics> devicePowerSort = new TreeMap<>(maxPower);
        devicePowerSort.putAll(deviceMap);
        for (String key: devicePowerSort.keySet()) {
            System.out.println(devicePowerSort.get(key).toString());
        }

        System.out.println("\nСортировка по серии:");
        Comparator serial = new SerialComparator(deviceMap);
        Map<String, Electronics> deviceSerialSort = new TreeMap<>(serial);
        deviceSerialSort.putAll(deviceMap);
        deviceSerialSort.forEach((k, v) -> System.out.println(v.toString()));

        System.out.println("\nСортировка по статусу:");
        Comparator status = new StatusComparator(deviceMap);
        Map<String, Electronics> deviceStatusSort = new TreeMap<>(status);
        deviceStatusSort.putAll(deviceMap);
        deviceStatusSort.forEach((k, v) -> System.out.println(v.toString()));

        System.out.println("Поиск устройства: ");
        controller.searchDevice(2);
    }
}
