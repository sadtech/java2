package org.sadtech.java2.apartment;

import org.sadtech.java2.abstraction.Device;
import org.sadtech.java2.abstraction.Electronics;

import java.util.Map;

public class Controller implements Electronics {
    private Map<String, Electronics> devices;
    private float sumPower;

    public Controller(Map<String, Electronics> proxied) {
        this.devices = proxied;
        sumPower = 0;
    }

    public void turnON() {
        System.out.println("Выберете устройство, которое желаете включить!");
    }

    public void turnON(String key) {
        if (checkTurnON(key)) {
            System.out.println("Устройство " + key + " уже включено!");
        } else {
            devices.get(key).turnON();
            sumPower += devices.get(key).getMaxPower();
            System.out.println("Устройство " + key + " включено!");
        }
    }

    private boolean checkTurnON(String key) {
        if (devices.get(key).isStatus()) {
            return true;
        } else {
            return false;
        }
    }

    public void switchOFF() {
        System.out.println("Выберете устройство, которое желаете выключить!");
    }

    public void switchOFF(String key) {
        if (checkTurnON(key) == false) {
            System.out.println("Устройство" + key + " уже выключено");
        } else {
            devices.get(key).switchOFF();
            sumPower -= devices.get(key).getMaxPower();
            System.out.println("Устройство " + key + " выключено!");
        }
    }

    public boolean isStatus() {
        return true;
    }

    public float getMaxPower() {
        return sumPower;
    }

    public void searchDevice(int serial) {
        for (String key : devices.keySet()) {
            if (devices.get(key).getSerial() == serial) {
                System.out.println("Устройство с серийным номером " + serial + " найдено. Это " + key);
            }
        }
    }

    @Override
    public int getSerial() {
        return 0;
    }
}
