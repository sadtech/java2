package org.sadtech.java2.comparators;

import org.sadtech.java2.abstraction.Electronics;

import java.util.Comparator;
import java.util.Map;

public class StatusComparator implements Comparator<String> {
    private Map<String, Electronics> map;

    public StatusComparator(Map<String, Electronics> map) {
        this.map = map;
    }

    @Override
    public int compare(String key1, String key2) {
        if (map.get(key1).isStatus() != map.get(key2).isStatus()) {
            return -1;
        } else {
            return 1;
        }
    }
}
