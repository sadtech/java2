package org.sadtech.java2.comparators;

import org.sadtech.java2.abstraction.Electronics;

import java.util.Comparator;
import java.util.Map;

public class MaxPowerComparator implements Comparator<String> {
    private Map<String, Electronics> map;

    public MaxPowerComparator(Map<String, Electronics> map) {
        this.map = map;
    }

    @Override
    public int compare(String key1, String key2) {
        return ((int) (map.get(key1).getMaxPower() - map.get(key2).getMaxPower()));
    }
}
