package org.sadtech.java2.comparators;

import org.sadtech.java2.abstraction.Device;
import org.sadtech.java2.abstraction.Electronics;

import java.util.Comparator;
import java.util.Map;

public class SerialComparator implements Comparator<String> {
    private Map<String, Electronics> map;

    public SerialComparator(Map<String, Electronics> map) {
        this.map = map;
    }

    public int compare(String key1, String key2) {
        return ((Device) map.get(key1)).getSerial() - ((Device) map.get(key2)).getSerial();
    }
}
